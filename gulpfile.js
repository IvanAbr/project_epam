var gulp        = require('gulp'),
    sass        =  require('gulp-sass'),
    notify      = require('gulp-notify'),
    browserSync = require('browser-sync');
    // concat = require('gulp-concat'),
    // cssnano = require('gulp-cssnano'),
    // rename = require('gulp-rename'),
    // del = require('del'),
    // imagemin = require('gulp-imagemin'),
    // pngquant = require('imagemin-pngquant'),
    // cache = require('gulp-cache'),
    //autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function () {
    return gulp.src('app/sass/style.scss')
        .pipe(sass().on('error', notify.onError({title: 'sass'})))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false
    });
});

gulp.task('css-libs', ['sass'], function () {
    return gulp.src('app/css/*.css')
        .pipe(concat('style.css'))
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('app/css'))

});

gulp.task('watch', ['browser-sync', 'css-libs'], function () {
    gulp.watch('app/sass/**/*.scss', [sass], browserSync.reload);
    gulp.watch('app/*.html', browserSync.reload);
    gulp.watch('app/js/**/*.js', browserSync.reload);
});

// gulp.task('clean', function () {
//     return del.sync('dist')
// });

// gulp.task('cleancss', function () {
//     return del.sync(['app/css/*.css', '!app/css/style.css', '!app/css/normalize.css'])
// });

// gulp.task('img', function () {
//     return gulp.src('app/img/**/*')
//         .pipe(cache(imagemin({
//             interlaced: true,
//             progressive: true,
//             svgoPlugins: [{removeViewBox: false}],
//             use: [pngquant()]
//         })))
//         .pipe(gulp.dest('dist/img')); // Выгружаем на продакшен
// });

// gulp.task('build', ['clean', 'img', 'sass', 'css-libs'], function () {
//     var buildCss = gulp.src([
//         'app/css/*.css',
//         'app/css/*.min.css'
//     ])
//         .pipe(gulp.dest('dist/css'))
//     var buildFonts = gulp.src('app/fonts/**/*')
//         .pipe(gulp.dest('dist/fonts'))
//     var buildJs = gulp.src('app/js/**/*')
//         .pipe(gulp.dest('dist/js'))
//     var buildHtml = gulp.src('app/*.html')
//         .pipe(gulp.dest('dist'));
// });

// gulp.task('clear', function () {
//     return cache.clearAll();
// });

// gulp.task('default', ['watch']);